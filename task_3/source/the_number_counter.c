#include <stdio.h>
#include <stdbool.h>
#include <limits.h>
//A program to calculate the sum, average, smallest number and biggest number of the numbers entered.

int main(int argc, char** argv)
{
    int sum;
    int entered_number;
    int number_counter;
    int smallest;
    int biggest;
    float average;
    bool x;
    int run = 1;

    printf("\nWelcome to the number counter!\n");
    while (run)
    {
        sum = 0;
        entered_number = 0;
        number_counter = 0;
        smallest = INT_MAX;
        biggest = 0;
        average = 0.0;
        x = true;

        printf("\nEnter a negative number to stop.\n");

        while (x)
        {
            printf("Enter a positive integer: ");
            scanf("%d", &entered_number);

            if (entered_number >= 0)
            {
                sum=sum+entered_number;
                number_counter++;
                if (entered_number > biggest)
                {
                    biggest = entered_number;
                }

                if (entered_number < smallest)
                {
                    smallest = entered_number;
                }
            }

            else 
            {
                if (!(number_counter > 0))
                {
                    printf("\nYou stopped immediately, no valid numbers were entered for calculation.\n");
                    x = false;
                }
                else
                {
                    printf("\n");
                    printf("The smallest number you entered was: %d\n", smallest);
                    printf("The biggest number you entered was: %d\n", biggest);
                    printf("The sum of the numbers you entered was: %d\n", sum);
                    average = sum/number_counter;
                    printf("The average of the numbers you entered was: %.1f\n", average);
                    x = false;
                }
            }
        }

        printf("\nWould you like to run the program again? (1 for yes, 0 for no): ");
        scanf("%d", &run);
    }


    return 0;
}
