#include <stdio.h>

//A program that prints if the angle entered is right, acute or obtuse.

int main(int argc, char** argv)
{
    int angle;

    printf("please enter a positive angle in degrees(only integers in the range 0 to 180 are accepted): ");
    scanf("%d", &angle);

    if (angle == 90)
    {
        printf("the angle %d is right.\n", angle);
    }
    else if (angle < 90 && angle >= 0)
    {
        printf("the angle %d is acute.\n", angle);
    }
    else if (angle > 90 && angle <= 180)
    {
        printf("the angle %d is obtuse.\n", angle);
    }
    else
    {
        printf("incorrect input!\n");
    }

    return 0;
}
