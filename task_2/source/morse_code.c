#include <stdio.h>

//A program that prints the morse code of the number entered.

int main(int argc, char** argv)
{
    int number;

    printf("please enter an integer number between 0 and 5: ");
    scanf("%d", &number);

    if (number >= 0 && number <= 5)
    {
        switch(number)
        {
            case 0: printf("-----\n"); break;
            case 1: printf(".----\n"); break;
            case 2: printf("..---\n"); break;
            case 3: printf("...--\n"); break;
            case 4: printf("....-\n"); break;
            case 5: printf(".....\n"); break;
        }
    }
    else
    {
        printf("error, incorrect input\n");
    }


    return 0;
}
